#!/bin/bash

pluto update planet.ini
ruby -r 'jekyll/planet' -e 'JekyllPlanet.main'

# Workaround for no https support on Marco Martin's and potentially other blogs
imgcachedir="img/cache"
mkdir -p $imgcachedir

for post in _posts/*; do
	# Extract image urls and download them to the images folder
	# Then update all image urls in the posts
	for imageurl in $(grep -oP '<img\s+src="\K[^"]+' $post | grep 'http'); do
		image_target_filename="$imgcachedir/$(echo $imageurl | sed 's,/,_,g')"

		wget --continue $imageurl -O $image_target_filename

		sed -i "s,$imageurl,/$image_target_filename,g" $post
	done
done

jekyll build
