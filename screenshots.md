---
title: Screenshots
permalink: /screenshots/
layout: screenshots
---

* ### Plasma ![shell.png](/img/screenshots/shell.png)

* ### Angelfish ![angelfish.png](/img/screenshots/angelfish.png)

* ### Settings ![settings.png](/img/screenshots/settings.png)

* ### Kaidan ![kaidan_1.png](/img/screenshots/kaidan_1.png)

* ### Pure Maps ![puremaps.png](/img/screenshots/puremaps.png)

* ### Okular ![okular.png](/img/screenshots/okular.png)

* ### Koko ![koko.png](/img/screenshots/koko.png)

* ### Index ![index.png](/img/screenshots/index.png)

* ### Calindori ![calindori.png](/img/screenshots/calindori.png)

* ### Discover ![discover.png](/img/screenshots/discover.png)

* ### Angelfish ![angelfish_2.png](/img/screenshots/angelfish_2.png)

* ### Kalgebra ![kalgebra.png](/img/screenshots/kalgebra.png)

* ### Camera ![camera.png](/img/screenshots/camera.png)

* ### Kirigami Gallery ![kirigami_gallery.png](/img/screenshots/kirigami_gallery.png)

* ### Angelfish ![angelfish_3.png](/img/screenshots/angelfish_3.png)

* ### Kaidan ![kaidan_2.png](/img/screenshots/kaidan_2.png)

* ### Calindori ![calindori_2.png](/img/screenshots/calindori_2.png)
